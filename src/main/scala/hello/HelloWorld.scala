package hello

/**
  * Hello World を表示するプログラム
  */
object HelloWorld {

  /**
    * Hello World 表示メイン処理
    * @param args コマンドライン引数
    */
  def main(args : Array[String]) = {
    println("Hello World")
  }
}
